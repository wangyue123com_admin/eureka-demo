# eureka-demo

#### 介绍
当前项目为springboot集成eureka简单使用，实际应用中可以将多个客户端部署至多个服务器，来达到高可用和提升服务器承载量。

#### 软件架构
sprintboot + eureka


#### 安装教程

1.  启动eureka-server的Application
2.  启动eureka-clinet1的Application
3.  启动eureka-clinet2的Application
4.  启动eureka-wx-api的Application
5.  访问http://127.0.0.1:8088/person/list

#### 使用说明

1.  启动eureka-server的Application
2.  启动eureka-clinet1的Application
3.  启动eureka-clinet2的Application
4.  启动eureka-wx-api的Application
5.  访问http://127.0.0.1:8088/person/list

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
