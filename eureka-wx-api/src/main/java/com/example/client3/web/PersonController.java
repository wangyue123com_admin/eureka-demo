package com.example.client3.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RequestMapping("/person")
@RestController
public class PersonController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/list")
    public Map getPersion(){
        Map forObject = restTemplate.getForObject("http://service-1/person/list", Map.class);
        return forObject;
    }
}
