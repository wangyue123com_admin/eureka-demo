package com.example.clinet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = "com")
@EnableDiscoveryClient //标识启用服务注册和发现
public class ClinetApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClinetApplication.class, args);
    }

}
